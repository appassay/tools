# README

## Terminology and structure

* There is a questionnaire. It contains the questions being asked about an app.
  It is defined in XML.
* There is the assay data. It contains the answers about an app.
  It is defined in XML.
* There is an assay. It is instantiated from an questionnaire, and the assay data.
* There is a layout. It defines which questions and answers go into which pages, in
  which sequence. It is defined in XML.
* There is a document set. It is instantiated by applying an assay to a layout.
* It can be saved (N documents with different names)

## Notes on how to fill out assays

* The values go into ``<value></value>`` sections.
* If there is more than one value (arity>1), use multiple ``<value></value>`` sections.
* If you like to answer a question but insufficient information is available, answer
  as ``<value>{{% unknown %}}</value>``
* If a question does not have an answer or does not apply, remove the ``<value>`` tags.
  Depending on the item, this should either not generate any information for the site
  (e.g. if there is no local-language website, it isn't mentioned at all on the website)
  or text such as ``N/A``.
