#!/usr/bin/env python
#
# Given the assay questionnaire, generate markdown for it
#

import argparse
import datetime
import os.path
from appassay.questionnaire import Questionnaire

now = datetime.datetime.utcnow()

parser = argparse.ArgumentParser()
parser.add_argument( 'questionnaire', help='The assay questionnaire XML file to convert' )
parser.add_argument( '--output',      help='The output markdown file to write, defaults to questionnaire filename with extension .md' )

args = parser.parse_args()

questionnaire = Questionnaire.readFromXmlFile( args.questionnaire );

if args.output is None :
    if args.questionnaire.endswith( '.xml' ) :
        markdownFile = "%s.md" % args.questionnaire[ 0 : -4 ]
    else :
        markdownFile = "%s.md" % args.questionnaire
else :
    markdownFile = args.output

if os.path.isfile( markdownFile ) :
    raise Exception( 'ERROR: file exists already, not overwriting: %s' % markdownFile )

with open( markdownFile, 'w' ) as out:
    out.write( '---\n' )
    out.write( 'title: Questionnaire for an App\n' )
    out.write( 'weight: 40\n' )
    out.write( 'lastmod: %s\n' % now.strftime( '%Y-%m-%dZ%H:%M:%SZ'))
    out.write( '---\n' )
    out.write( '\n' )
    out.write( '{{%% methodology-version %%}}%s{{%% /methodology-version %%}}\n' %  questionnaire.getVersion() )
    out.write( "Note: For Capitalized Terms consult our {{% pageref \"/glossary\" %}}.\n")
    out.write( '\n' )

    for e in questionnaire.getEntries() :
        out.write( e.asQuestionnaireMarkdown() )
        out.write( "\n" )

