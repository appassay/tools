#!/usr/bin/env PYTHONPATH=tools/python python
#
# Represents an assay for a particular app
#

class Assay :
    """
    The assay
    """

    class Answer :
        """
        A content element in the Assay that is an Answer.
        """

        def __init__( self, assay, answerData, question ) :
            """
            answerData: pointer to the AnswerData element from the AssayData
            question: pointer to the Question from the Questionnaire
            """
            self.assay      = assay
            self.answerData = answerData
            self.question   = question

        def getAssay( self ) :
            return self.assay


        def getId( self ) :
            return self.answerData.getId()


        def getQuestion( self ) :
            return self.question


        def asFrontMatterValue( self ) :
            return self.question.getAnswerType().createFrontMatterValueFrom(
                    self.answerData.getXmlAnswerNodes(),
                    self.question.getId() )


        def asAssayMarkdown( self ) :
            return self.question.getAnswerType().createMarkdownValueFrom(
                    self.answerData.getXmlAnswerNodes(),
                    self.question.getId() )


    def __init__( self, questionnaire, assayData ) :
        """
        template: the Template instance this Assay is an instance of
        """
        self.questionnaire = questionnaire
        self.assayData     = assayData
        self.answers       = None


    def getAppId( self ) :
        return self.assayData.getAppId()


    def getQuestionnaire( self ) :
        return self.questionnaire


    def getAnswers( self ) :
        if self.answers is None :
            self.answers     = {}
            answersData = self.assayData.getAnswersData() # hash of id to AssayData.AnswerData
            questions   = self.questionnaire.getQuestions()

            for q in questions.values() :
                if q.getId() in answersData :
                    answer = Assay.Answer( self, answersData[ q.getId() ], q )
                    self.answers[ answer.getId() ] = answer

                else :
                    raise Exception( 'ERROR: no answers data available for question with id: %s' % q.getId() )

            # make sure there are no superfluous answers
            for answerData in answersData.values() :
                if not answerData.getId() in questions :
                    raise Exception( 'ERROR: no question corresponds to answers data with id: %s' % answerData.getId() )

        return self.answers

