#!/usr/bin/env PYTHONPATH=tools/python python
#
# Represents a questionnaire used for assays
#

from appassay.answertype import AnswerType
from appassay.assay import Assay
from appassay.utils import markdownUlify
import hashlib
import os.path
import re
from xml.dom import minidom
from xml.parsers.expat import ExpatError, ErrorString

class Questionnaire :
    """
    The assay questionannaire
    """

    class Element :
        """
        A content element in the Questionnaire, such as a Question or a Headline
        """
        def __init__( self, questionnaire ) :
            self.questionnaire = questionnaire


        def getId( self ) :
            """
            Obtain something that is unique among elements of this type.
            None means ignore.
            """
            return None


        def asQuestionnaireMarkdown( self ) :
            """
            Obtain a Markdown representation of this Element for the questionnaire output
            """
            pass


        def asAssayMarkdown( self ) :
            """
            Obtain a Markdown representation of this Element for the assay output
            """
            pass


        def asAssayXml( self ) :
            """
            Obtain an XML representation of this Element suitable for an assay file
            """
            pass



    class Headline( Element ) :
        """
        A content element in the Questionnaire that is a Headline
        """

        @staticmethod
        def createFromXmlNode( questionnaire, xmlNode ) :
            """
            Factory method, from an XML node
            """
            headerMatch = re.search( "^h(\d+)$", xmlNode.tagName )
            return Questionnaire.Headline( int( headerMatch.group(1) ), xmlNode.childNodes[0].data.strip(), questionnaire )


        def __init__( self, level, text, questionnaire ) :
            """
            level: level of the headline, e.g. 3 for an H3 entry
            text: the actual headline
            """
            super().__init__( questionnaire )

            self.level = level
            self.text  = text


        def asQuestionnaireMarkdown( self ) :
            return "%s %s\n" % ( ( '#' * ( self.level + 1 )), self.text )


        def asAssayMarkdown( self ) :
            raise Exception( 'ERROR: should not have been invoked' )


        def asAssayXml( self ) :
            return "   <h%d>%s</h%d>\n" % ( self.level, self.text, self.level )


    class Question( Element ) :
        """
        A content element in the Questionnaire that is a Question.
        """

        @staticmethod
        def createFromXmlNode( questionnaire, xmlNode ) :
            """
            Factory method, from an XML node
            """
            id = xmlNode.getAttribute( 'id' )
            if id is None :
                raise Exception( 'ERROR: Question does not have an id attribute' )

            text       = None
            answerType = None
            for childNode in xmlNode.childNodes :
                if isinstance( childNode, minidom.Element ):
                    if childNode.tagName == 'text' :
                        if text is not None:
                            raise Exception( 'ERROR: more than one text child for question ' + id )
                        text = childNode.childNodes[0].data.strip()

                    else :
                        answerType2 = AnswerType.createFromXmlNode( childNode )
                        if answerType is not None and answerType2 is not None :
                            raise Exception( 'ERROR: more than one answer type for question ' + id )
                        answerType = answerType2

            return Questionnaire.Question( questionnaire, id, text, answerType )


        def __init__( self, questionnaire, id, text, answerType ) :
            """
            id: identifier of the question in the questionnaire
            text: the text of the question
            answerType: the AnswerType for the question
            """
            super().__init__( questionnaire )
            self.id         = id
            self.text       = text
            self.answerType = answerType


        def getId( self ) :
            return self.id


        def getText( self ) :
            return self.text


        def asQuestionnaireMarkdown( self ) :
            return markdownUlify( self.getText() )


        def asAssayMarkdown( self ) :
            return self.getText()


        def asAssayXml( self ) :
            ret  = '   <question id="%s">\n' % self.id
            ret += '    <text>\n'
            ret += self.text + '\n'
            ret += '    </text>\n'
            ret += self.answerType.asAssayXml()
            ret += '   </question>\n'
            return ret


        def getAnswerType( self ) :
            return self.answerType


    @staticmethod
    def readFromXmlFile( xmlFileName ) :
        """
        Factory method, from the provided XML Questionnaire file
        """
        if not os.path.isfile( xmlFileName ) :
            raise Exception( 'ERROR: cannot find questionnaire XML file: ' + xmlFileName )

        try :
            xmlDoc            = minidom.parse( xmlFileName )
            xmlQuestionnaires = xmlDoc.getElementsByTagName( 'questionnaire' )

        except ExpatError as e:
            raise Exception( 'ERROR: invalid XML document %s (line: %d, offset: %d): %s' % ( xmlFileName, e.lineno, e.offset, ErrorString( e.code )))

        if xmlQuestionnaires.length != 1 :
            raise Exception( 'ERROR: not a valid questionnaire document: ' + xmlFileName )

        return Questionnaire( xmlQuestionnaires[0] )


    def __init__( self, xmlQuestionnaire ) :
        """
        Constructor. Use factory method instead.
        xmlQuestionnaire: the XML document top-level node
        """
        self.xmlQuestionnaire = xmlQuestionnaire
        self.entries          = None # array of entries in the sequence in which they were specified
        self.questionsHash    = None # hash of id to question


    def getVersion( self ) :
        """
        Obtain the version specified in the Questionnaire file.
        """
        xmlMetas = self.xmlQuestionnaire.getElementsByTagName( 'meta' )
        if xmlMetas.length != 1 :
            print( 'WARNING: no or invalid meta entry' );
            return None
        xmlVersions = xmlMetas[0].getElementsByTagName( 'version' )
        if  xmlVersions.length != 1 :
            print( 'WARNING: no or invalid meta/version entry' )
            return None
        xmlVersion = xmlVersions[0]
        if xmlVersion.childNodes.length != 1 :
            print( 'WARNING: no or invalid meta/version entry' )
            return None

        return xmlVersion.childNodes[0].data


    def getEntries( self ) :
        """
        Obtain the entries specified in the questionnnaire file, in sequence.
        """
        if self.entries is None:
            xmlEntries = self.xmlQuestionnaire.getElementsByTagName( 'entries' )
            if xmlEntries.length != 1 :
                raise Exception( 'ERROR: no or invalid entries element' )

            self.entries       = []
            self.questionsHash = {}

            for xmlEntry in xmlEntries[0].childNodes :
                if not isinstance( xmlEntry, minidom.Element ):
                    continue

                newEntry = None

                headerMatch = re.search( "^h\d+$", xmlEntry.tagName )

                if headerMatch is not None:
                    newEntry = Questionnaire.Headline.createFromXmlNode( self, xmlEntry )

                elif xmlEntry.tagName == 'question' :
                    newEntry = Questionnaire.Question.createFromXmlNode( self, xmlEntry )

                if newEntry is not None :
                    if newEntry.getId() is not None and newEntry.getId() in self.questionsHash:
                        raise Exception( 'ERROR: id not unique: %s' % newEntry.getId() )

                    self.entries.append( newEntry )
                    self.questionsHash[ newEntry.getId() ] = newEntry

        return self.entries


    def getQuestions( self ) :
        ret = {}
        for ee in filter( lambda e: isinstance( e, Questionnaire.Question ), self.getEntries() ) :
            ret[ee.getId()] = ee

        return ret


    def instantiateAssay( self, assayData ) :
        """
        Given the data for an assay, and this questionnaire, instantiate the Assay.
        Bug out if there are inconsistencies
        """

        # first some checks
        templateVersion = self.getVersion()
        assayVersion    = assayData.getQuestionnaireVersion()

        if templateVersion is None:
            raise Exception( 'ERROR: questionnaire has no version' )
        if assayVersion is None:
            raise Exception( 'ERROR: assay data file does not have a auestionnaire version' )
        if templateVersion != assayVersion :
            raise Exception( 'ERROR: mismatching versions: questionnaire (%s) vs assay data (%s)' % ( templateVersion, assayVersion ))

        return Assay( self, assayData )
