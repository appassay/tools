#!/usr/bin/env PYTHONPATH=tools/python python
#
# Represents the data of an assay for a particular app
#

import os.path
from xml.dom import minidom
from xml.parsers.expat import ExpatError, ErrorString


class AssayData :
    """
    The data in an Assay xml file. This has not been matched with the Questionnaire yet.
    """

    class AnswerData :
        """
        One actual answer in the Assay xml file. Ignores everything else.
        """
        @staticmethod
        def createFromXmlNode( assayData, node ) :
            """
            Factory method, from an XML node
            """
            id = node.getAttribute( 'id' )
            if id is None :
                raise Exception( 'ERROR: Question does not have an id attribute' )

            xmlAnswerNodes = []
            for childNode in node.childNodes :
                if not isinstance( childNode, minidom.Element ):
                    continue

                if childNode.tagName == 'answer' :
                    xmlAnswerNodes.append( childNode )

            return AssayData.AnswerData( assayData, id, xmlAnswerNodes )


        def __init__( self, assayData, id, xmlAnswerNodes ) :
            """
            xmlAnswerNodes: array, usually of length 1, but in case of arity="*" it can be shorter or longer
            """
            self.assayData      = assayData
            self.id             = id
            self.xmlAnswerNodes = xmlAnswerNodes


        def getAssayData( self ) :
            return self.assayData


        def getId( self ) :
            return self.id


        def getXmlAnswerNodes( self ) :
            return self.xmlAnswerNodes


    @staticmethod
    def readFromXmlFile( xmlFileName ) :
        """
        Factory method, from the provided XML assay file
        """
        if not os.path.isfile( xmlFileName ) :
            raise Exception( 'ERROR: cannot find assay XML file: ' + xmlFileName )

        try :
            xmlDoc    = minidom.parse( xmlFileName )
            xmlAssays = xmlDoc.getElementsByTagName( 'assay' )

        except ExpatError as e:
            raise Exception( 'ERROR: invalid XML document %s (line: %d, offset: %d): %s' % ( xmlFileName, e.lineno, e.offset, ErrorString( e.code )))

        if xmlAssays.length != 1 :
            raise Exception( 'ERROR: not a valid assay document: ' + xmlFileName )

        return AssayData( xmlAssays[0] )


    def __init__( self, xmlAssay ) :
        """
        Constructor. Use factory method instead.
        xmlAssay: the XML document top-level node
        """
        self.xmlAssay    = xmlAssay
        self.answersData = None


    def getAppId( self ) :
        ret = self.xmlAssay.getAttribute( 'appid' )
        if ret is None or len( ret ) == 0 :
            print( 'WARNING: no appid given in app data' )
            ret = "UNKNOWN"
        return ret


    def getQuestionnaireVersion( self ) :
        """
        Obtain the version specified in the questionnaire file.
        """
        xmlQuestionnnaires = self.xmlAssay.getElementsByTagName( 'questionnaire' )
        if xmlQuestionnnaires.length != 1 :
            print( 'WARNING: no or invalid questionnaire entry' );
            return None
        xmlMetas = xmlQuestionnnaires[0].getElementsByTagName( 'meta' )
        if xmlMetas.length != 1 :
            print( 'WARNING: no or invalid meta entry' );
            return None
        xmlVersions = xmlMetas[0].getElementsByTagName( 'version' )
        if  xmlVersions.length != 1 :
            print( 'WARNING: no or invalid meta/version entry' )
            return None
        xmlVersion = xmlVersions[0]
        if xmlVersion.childNodes.length != 1 :
            print( 'WARNING: no or invalid meta/version entry' )
            return None

        return xmlVersion.childNodes[0].data


    def getAnswersData( self ) :
        """
        Return the data for the answers contained in this assay. Ignore everything else.
        """
        if self.answersData is None:

            xmlEntries = self.xmlAssay.getElementsByTagName( 'entries' )
            if xmlEntries.length != 1 :
                raise Exception( 'ERROR: no or invalid entries element' )

            self.answersData = {}

            for xmlEntry in xmlEntries[0].childNodes :
                if not isinstance( xmlEntry, minidom.Element ):
                    continue

                if not xmlEntry.tagName == 'question' :
                    continue # everything else ignored

                newAnswersData = AssayData.AnswerData.createFromXmlNode( self, xmlEntry )

                if newAnswersData.getId() in self.answersData:
                    raise Exception( 'ERROR: id not unique: %s' % newAnswersData.getId() )

                self.answersData[ newAnswersData.getId() ] = newAnswersData

        return self.answersData
