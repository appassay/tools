#!/usr/bin/env PYTHONPATH=tools/python python
#
# Defines the data type for an answer in a Assay.
#

from appassay.utils import markdownUlify
from xml.dom import minidom


class AnswerType :
    """
    Supertype of all possible types for answers. Incomplete, FIXME
    """

    @staticmethod
    def createFromXmlNode( xmlNode ) :
        if not xmlNode.tagName.endswith( 'type' ) :
            return None

        if xmlNode.tagName == 'stringtype' :
            return StringAnswerType( xmlNode )

        if xmlNode.tagName == 'roletype' :
            return RoleAnswerType( xmlNode )

        if xmlNode.tagName == 'enumtype' :
            return EnumAnswerType( xmlNode )

        return UninterpretedTextForNowAnswerType( xmlNode )


    def __init__( self, xmlNode ) :
        self.xmlNode = xmlNode


    def getArity( self ) :
        arity = self.xmlNode.getAttribute( 'arity' )
        if arity is None :
            return '1'
        if arity == '' :
            return '1'
        return arity


    def asAssayXml( self ) :
        ret =  '    ' + self.xmlNode.toxml().replace( '\n', '\n    ') + '\n'
        ret += '    <answer>\n'
        ret += '    </answer>\n'
        return ret


    def createSingleFrontMatterValueFrom( self, xmlAnswerNode, answerId ) :
        pass


    def createSingleMarkdownValueFrom( self, xmlAnswerNode, answerId ) :
        pass


    def createFrontMatterValueFrom( self, xmlAnswerNodes, answerId ) :
        if self.getArity() == '1' :
            if len( xmlAnswerNodes ) > 0 :
                ret = self.createSingleFrontMatterValueFrom( xmlAnswerNodes[0], answerId )
            else :
                ret = None

        else :
            values = map(
                    lambda x: self.createSingleFrontMatterValueFrom( x, answerId ),
                    xmlAnswerNodes )
            values = map(
                    lambda x: "'%s'" % x,
                    values )
            ret = '[ %s ]' % ', '.join( values )

        return ret


    def createMarkdownValueFrom( self, xmlAnswerNodes, answerId ) :
        if self.getArity() == '1' :
            if len( xmlAnswerNodes ) > 0 :
                ret = self.createSingleMarkdownValueFrom( xmlAnswerNodes[0], answerId )
            else :
                ret = None

        else :
            if len( xmlAnswerNodes ) == 0 :
                ret = None

            else :
                values = map(
                        lambda x: markdownUlify( self.createSingleMarkdownValueFrom( x, answerId )),
                        xmlAnswerNodes )
                ret = "\n".join( values )

        return ret


class StringAnswerType( AnswerType ) :
    def __init__( self, xmlNode ) :
        AnswerType.__init__( self, xmlNode )


    def createSingleFrontMatterValueFrom( self, xmlAnswerNode, answerId ) :
        ret = xmlAnswerNode.childNodes[0].data.strip()
        if not ret:
            raise Exception( 'ERROR: no or blank value in answer for %s' % answerId )
        return ret


    def createSingleMarkdownValueFrom( self, xmlAnswerNode, answerId ) :
        ret = xmlAnswerNode.childNodes[0].data.strip()
        if not ret:
            raise Exception( 'ERROR: no or blank value in answer for %s' % answerId )
        return ret


class RoleAnswerType( AnswerType ) :
    def __init__( self, xmlNode ) :
        AnswerType.__init__( self, xmlNode )


    def createSingleFrontMatterValueFrom( self, xmlAnswerNode, answerId ) :
        player      = None
        description = None

        for childNode in xmlAnswerNode.childNodes :
            if childNode.tagName == 'roleplayer' :
                player = childNode.childNodes[0].data.strip()

            elif childNode.tagName == 'roledescription' :
                description = childNode.childNodes[0].data.strip()

        if not description or not player :
            raise Exception( 'ERROR: no or empty roleplayer or roledescription in answer for %s' % answerId )

        return "%s -> %s" % ( description, player )


    def createSingleMarkdownValueFrom( self, xmlAnswerNode, answerId ) :
        player      = None
        description = None

        for childNode in xmlAnswerNode.childNodes :
            if not isinstance( childNode, minidom.Element ):
                continue

            if childNode.tagName == 'roleplayer' :
                player = childNode.childNodes[0].data.strip()

            if childNode.tagName == 'roledescription' :
                description = childNode.childNodes[0].data.strip()

        if not description or not player :
            raise Exception( 'ERROR: no or empty roleplayer or roledescription in answer for %s' % answerId )

        return "%s: %s" % ( description, player )


class EnumAnswerType( AnswerType ) :
    def __init__( self, xmlNode ) :
        AnswerType.__init__( self, xmlNode )


    def createSingleFrontMatterValueFrom( self, xmlAnswerNode, answerId ) :
        ret = xmlAnswerNode.childNodes[0].data.strip()
        if not ret:
            raise Exception( 'ERROR: no or blank value in answer for %s' % answerId )
        return ret


    def createSingleMarkdownValueFrom( self, xmlAnswerNode, answerId ) :
        ret = xmlAnswerNode.childNodes[0].data.strip()
        if not ret:
            raise Exception( 'ERROR: no or blank value in answer for %s' % answerId )
        return ret


class UninterpretedTextForNowAnswerType( AnswerType ) :
    """
    Hopefully we can dissect this a bit more at some point :-) FIXME.
    """
    def __init__( self, xmlNode ) :
        AnswerType.__init__( self, xmlNode )


    def createSingleFrontMatterValueFrom( self, xmlAnswerNode, answerId ) :
        ret = xmlAnswerNode.childNodes[0].data.strip()
        if not ret:
            raise Exception( 'ERROR: no or blank value in answer for %s' % answerId )
        return ret


    def createSingleMarkdownValueFrom( self, xmlAnswerNode, answerId ) :
        ret = xmlAnswerNode.childNodes[0].data.strip()
        if not ret:
            raise Exception( 'ERROR: no or blank value in answer for %s' % answerId )
        return ret

