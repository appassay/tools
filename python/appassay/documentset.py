#!/usr/bin/env PYTHONPATH=tools/python python
#
# A set of documents created by instantiating a Layout with an Assay

import datetime
import os.path


class DocumentSet :
    """
    A set of one or more Documents
    """

    class FrontMatterItemInDocument :
        def __init__( self, documentSet, frontMatterItem, value ) :
            self.documentSet     = documentSet
            self.frontMatterItem = frontMatterItem
            self.value           = value


        def saveToStream( self, out, questionsCovered = None ) :
            """
            out: the stream to save the item to
            """
            if self.value is None :
                if self.frontMatterItem.getSkipEmpty() :
                    return

                out.write( "%s: \n" % ( self.frontMatterItem.getFrontMatterName() ))

            else :
                value = self.value.replace( '\n', ' ')
                out.write( "%s: %s\n" % ( self.frontMatterItem.getFrontMatterName(), value ))

            if questionsCovered is not None :
                questionsCovered[ self.frontMatterItem.getAnswerId() ] = 1


    class SectionInDocument :
        def __init__( self, documentSet, section, answersInSectionInDocument ) :
            self.documentSet                = documentSet
            self.section                    = section
            self.answersInSectionInDocument = answersInSectionInDocument


        def saveToStream( self, out, questionsCovered = None ) :
            """
            out: the stream to save the section to
            """
            if len( self.answersInSectionInDocument ) == 0 and self.section.getSkipEmpty():
                return

            title     = self.section.getSectionTitle()
            shortcode = self.section.getShortcode()

            out.write( '{{%')
            out.write( ' %s' % shortcode )
            if title :
                out.write( ' title="%s"' % title )
            out.write( '%}}\n' )

            for answerInSectionInDocument in self.answersInSectionInDocument :
                answerInSectionInDocument.saveToStream( out, questionsCovered )

            out.write( '{{%% /%s %%}}\n' % shortcode )


    class AnswerInSectionInDocument :
        """
        An answer within a section of a document
        """
        def __init__( self, documentSet, answerInSection, answer ) :
            self.documentSet      = documentSet
            self.answerInSection  = answerInSection
            self.answer           = answer


        def getAnswer( self ) :
            return self.answer


        def saveToStream( self, out, questionsCovered = None ) :
            """
            out: the stream to save the section to
            """
            answerIdString = 'answerid="%s"' % self.answer.getId()
            shortcode      = self.answerInSection.getShortcode()

            out.write( '{{%% %s-q %s %%}}\n' % ( shortcode, answerIdString ))
            out.write( self.answer.getQuestion().asAssayMarkdown() )
            out.write( '{{%% /%s-q %%}}\n' % shortcode )

            md = self.answer.asAssayMarkdown()
            if md :
                out.write( '{{%% %s-a %s %%}}\n' % ( shortcode, answerIdString ) )
                out.write( self.answer.asAssayMarkdown() )
                out.write( '{{%% /%s-a %%}}\n' % shortcode )
            else :
                out.write( '{{%% %s-a-empty %s %%}}\n' % ( shortcode, answerIdString ) )

            if questionsCovered is not None:
                questionsCovered[ self.answer.getId() ] = 1



    class Document :
        """
        A single page or document
        """

        def __init__( self, documentSet, page, frontMatterItems, sections ) :
            self.documentSet      = documentSet
            self.page             = page
            self.frontMatterItems = frontMatterItems
            self.sections         = sections


        def saveToFile( self, fileName ) :
            """
            fileName: the name of the file to save the Document to
            """
            with open( fileName, 'w' ) as out:
                self.saveToStream( out )


        def saveToStream( self, out ) :
            """
            fileName: the stream to save the Document to
            """

            questionsCovered = {}

            out.write( '---\n' )

            out.write( 'appid : %s\n' % self.documentSet.getAssay().getAppId() )
            out.write( 'methodologyversion : %s\n' % self.documentSet.getAssay().getQuestionnaire().getVersion() )
            #  out.write( 'layout : %s\n' % 'FIXME' )
            out.write( 'lastmod : %s\n' % datetime.datetime.utcnow().strftime( '%Y-%m-%dZ%H:%M:%SZ'))

            for pageElement in self.frontMatterItems :
                pageElement.saveToStream( out, questionsCovered )

            out.write( '---\n' )
            out.write( '\n' )

            for pageElement in self.sections :
                pageElement.saveToStream( out, questionsCovered )

            if self.page.getCoverAllQuestions() :
                allAnswers = self.documentSet.getAssay().getAnswers()
                for answerId in allAnswers :
                    if not answerId in questionsCovered :
                        print( "WARNING: question not covered in page %s: %s" % ( self.page.getName(), answerId ))


    def __init__( self, assay, layout ) :
        self.assay     = assay
        self.layout    = layout
        self.documents = None


    def getAssay( self ) :
        return self.assay


    def getLayout( self ) :
        return self.layout


    def getDocuments( self ) :

        if self.documents is None:
            self.documents = {} # hash of file name to document
            answers = self.assay.getAnswers() # hash

            for page in self.layout.getPages() :

                frontMatterItemsInDocument = []
                sectionsInDocument         = []

                for frontMatterItem in page.getFrontMatterItems() :
                    frontMatterItemsInDocument.append(
                            DocumentSet.FrontMatterItemInDocument(
                                    self,
                                    frontMatterItem,
                                    frontMatterItem.getValueAsFrontMatterValue( answers )))

                for section in page.getSections() :
                    answersInDocumentSection = []

                    for answerInSection in section.getAnswersInSection() :
                        if not answerInSection.getAnswerId() in answers :
                            raise Exception( 'ERROR: cannot find answer with id: %s' % answerInSection.getAnswerId() )

                        answersInDocumentSection.append(
                                DocumentSet.AnswerInSectionInDocument(
                                        self,
                                        answerInSection,
                                        answers[ answerInSection.getAnswerId() ] ))

                    sectionsInDocument.append(
                            DocumentSet.SectionInDocument(
                                    self,
                                    section,
                                    answersInDocumentSection ))

                self.documents[ page.getName() ] = DocumentSet.Document(
                        self,
                        page,
                        frontMatterItemsInDocument,
                        sectionsInDocument )

        return self.documents

