#!/usr/bin/env PYTHONPATH=tools/python python
#
# Represents a website (or other) layout for assays. When instantiated with an Assay,
# it turns into a DocumentSet.

from appassay.documentset import DocumentSet
import datetime
import os.path
import re
from xml.dom import minidom
from xml.parsers.expat import ExpatError, ErrorString


class Layout :
    """
    The layout
    """

    class Page :
        """
        A page in the layout
        """
        def __init__( self, layout, name, shortcode, coverAllQuestions, frontMatterItems, sections ) :
            """
            name: the name of the page as used as a file name, e.g. "_index.md"
            title: the title of the page as shown to the reader
            frontMatterItems: array of FrontMatterItemInPage
            sections: array of SectionInPage
            """
            self.layout            = layout
            self.name              = name
            self.coverAllQuestions = coverAllQuestions
            self.frontMatterItems  = frontMatterItems
            self.sections          = sections

            if shortcode :
                self.shortcode = shortcode
            else :
                self.shortcode = 'title-answer'


        def getLayout( self ) :
            return self.layout


        def getName( self ) :
            return self.name


        def getShortcode( self ) :
            return self.shortcode


        def getFrontMatterItems( self ) :
            return self.frontMatterItems


        def getSections( self ) :
            return self.sections


        def getCoverAllQuestions( self ) :
            return self.coverAllQuestions


    class PageItem :
        """
        Abstract supertype for all things on a page
        """
        def __init__( self, page, skipEmpty ) :
            self.page       = page
            self.skipEmpty = skipEmpty


        def getPage( self ) :
            return self.page


        def setPage( self, page ) :
            self.page = page


        def getSkipEmpty( self ) :
            return self.skipEmpty


    class FrontmatterItem( PageItem ) :
        """
        Properties for a page
        """
        def __init__( self, page, skipEmpty, frontMatterName ) :
            super().__init__( page, skipEmpty )

            self.frontMatterName = frontMatterName


        def getFrontMatterName( self ) :
            return self.frontMatterName


        def getAnswerId( self ) :
            return None


        def getValue( self, answers ) :
            pass


    class FrontmatterAnswerItem( FrontmatterItem ) :
        """
        Properties for a page
        """
        def __init__( self, page, skipEmpty, frontMatterName, answerId ) :
            """
            answerId: identifier of the question/answer
            """
            super().__init__( page, skipEmpty, frontMatterName )

            self.answerId = answerId


        def getAnswerId( self ) :
            return self.answerId


        def getValueAsFrontMatterValue( self, answers ) :
            if not self.answerId in answers :
                raise Exception( 'ERROR: cannot find answer with id: %s' % self.answerId )
            return answers[ self.answerId ].asFrontMatterValue()


    class FrontmatterConstantItem( FrontmatterItem ) :
        """
        Properties for a page
        """
        def __init__( self, page, skipEmpty, frontMatterName, constant ) :
            super().__init__( page, skipEmpty, frontMatterName )

            self.constant = constant


        def getConstant( self ) :
            return self.constant


        def getValueAsFrontMatterValue( self, answers ) :
            return self.getConstant()


    class Section( PageItem ) :
        """
        A section within a Page
        """
        def __init__( self, page, skipEmpty, sectionName, sectionTitle, shortcode, answers ) :
            """
            sectionName: the name of the section type as used programmatically
            answerIds: identifiers of the question/answer, in sequence
            """
            super().__init__( page, skipEmpty )

            self.sectionName  = sectionName
            self.sectionTitle = sectionTitle
            self.shortcode    = shortcode
            self.answers      = answers


        def getSectionName( self ) :
            return self.sectionName


        def getSectionTitle( self ) :
            return self.sectionTitle


        def getShortcode( self ) :
            if self.shortcode :
                return self.shortcode
            else :
                return self.page.getShortcode()


        def getAnswersInSection( self ) :
            return self.answers


    class AnswerInSection :
        """
        An answer within a Section
        """
        def __init__( self, section, skipEmpty, shortcode, answerId ) :
            self.section   = section
            self.skipEmpty = skipEmpty
            self.shortcode = shortcode
            self.answerId  = answerId


        def getSection( self ) :
            return self.section


        def setSection( self, section ) :
            self.section = section


        def getSkipEmpty( self ) :
            return self.skipEmpty


        def getShortcode( self ) :
            if self.shortcode :
                return self.shortcode
            else :
                return self.section.getShortcode()


        def getAnswerId( self ) :
            return self.answerId


    @staticmethod
    def readFromXmlFile( xmlFileName ) :
        """
        Factory method, from the provided XML layout file
        """
        if not os.path.isfile( xmlFileName ) :
            raise Exception( 'ERROR: cannot find assaylayout XML file: ' + xmlFileName )

        try :
            xmlDoc          = minidom.parse( xmlFileName )
            xmlAssayLayouts = xmlDoc.getElementsByTagName( 'assaylayout' )

        except ExpatError as e:
            raise Exception( 'ERROR: invalid XML document %s (line: %d, offset: %d): %s' % ( xmlFileName, e.lineno, e.offset, ErrorString( e.code )))

        if xmlAssayLayouts.length != 1 :
            raise Exception( 'ERROR: not a valid assaylayout document: ' + xmlFileName )

        layout = Layout( xmlAssayLayouts[0] )

        layout._checkConsistency()

        return layout


    def __init__( self, xmlAssayLayout ) :
        """
        Constructor. Use factory method instead.
        xmlAssayLayout: the XML document top-level node
        assay: the Assay object
        """
        self.xmlAssayLayout = xmlAssayLayout
        self.pages          = None


    def getPages( self ) :
        """
        Obtain the pages that this assay translates into using this layout
        """
        if self.pages is None:
            self.pages = []
            for pageXml in self.xmlAssayLayout.childNodes :
                if not isinstance( pageXml, minidom.Element ):
                    continue

                frontMatterItems = []
                sections         = []

                for pageElementXml in pageXml.childNodes :
                    if not isinstance( pageElementXml, minidom.Element ):
                        continue

                    if pageElementXml.tagName == 'frontmatter' :
                        if pageElementXml.getAttribute( 'answerid' ) :
                            frontMatterItems.append(
                                    Layout.FrontmatterAnswerItem(
                                            None,
                                            pageElementXml.getAttribute( 'skipempty' ),
                                            pageElementXml.getAttribute( 'name' ),
                                            pageElementXml.getAttribute( 'answerid' )))

                        elif pageElementXml.getAttribute( 'value' ) :
                            frontMatterItems.append(
                                    Layout.FrontmatterConstantItem(
                                            None,
                                            pageElementXml.getAttribute( 'skipempty' ),
                                            pageElementXml.getAttribute( 'name' ),
                                            pageElementXml.getAttribute( 'value' )))

                    elif pageElementXml.tagName == 'section' :
                        answers = []

                        for answerElementXml in pageElementXml.childNodes :
                            if not isinstance( answerElementXml, minidom.Element ):
                                continue

                            if answerElementXml.tagName == 'answer' :
                                answers.append(
                                        Layout.AnswerInSection(
                                                None,
                                                pageElementXml.getAttribute( 'skipempty' ),
                                                answerElementXml.getAttribute( 'shortcode' ),
                                                answerElementXml.getAttribute( 'id' )))
                            else :
                                raise Exception( 'ERROR: unexpected answerElementXml: %s' % answerElementXml.tagName )

                        newSection = Layout.Section(
                                None,
                                pageElementXml.getAttribute( 'skipempty' ),
                                pageElementXml.getAttribute( 'name' ),
                                pageElementXml.getAttribute( 'title' ),
                                pageElementXml.getAttribute( 'shortcode' ),
                                answers )

                        for answer in answers :
                            answer.setSection( newSection )

                        sections.append( newSection )

                    else :
                        raise Exception( 'ERROR: unexpected pageElementXml: %s' % pageElementXml.tagName )

                coverAllQuestions = pageXml.getAttribute( 'coverAllQuestions' )
                coverAllQuestions = True if coverAllQuestions else False

                newPage = Layout.Page(
                        self,
                        pageXml.getAttribute( 'name' ),
                        pageXml.getAttribute( 'shortcode' ),
                        coverAllQuestions,
                        frontMatterItems,
                        sections )

                for item in frontMatterItems :
                    item.setPage( newPage )

                for section in sections :
                    section.setPage( newPage )

                self.pages.append( newPage )

        return self.pages


    def instantiateDocumentSet( self, assay ) :
        """
        Instantiate this Layout, for this Assay, into a set of documents
        """
        return DocumentSet( assay, self )


    def _checkConsistency( self ) :
        pass

