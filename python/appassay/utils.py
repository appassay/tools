#!/usr/bin/env PYTHONPATH=tools/python python
#
# Uilities
#

def markdownUlify( x ) :
    """
    Convert a block of multiline text into a markdown * bullet
    """
    return '* ' + x.replace( '\n', '\n  ' ) + '\n'

