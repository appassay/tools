#!/usr/bin/python
#
# Copyright (C) 2020 and later, App Assay. All rights reserved. License: see package.
#

from setuptools import setup
import appassay

setup(name='appassay',
      author='Johannes Ernst',
      license='AGPLv3',
      description='Scripts to create and maintain app assays',
      url='http://gitlab.com/appassay/tools',
      packages=[
          'appassay'
      ],
      zip_safe=True)
